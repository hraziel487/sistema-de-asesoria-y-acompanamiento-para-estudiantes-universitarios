<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Pruebas unitarias

Las pruebas unitarias verifican si las unidades individuales de código funcionan como se esperaba. Por ello nosotros hemos realizado diversas pruebas unitarias.

- **[Login]**
- **[Duplicacion]**
- **[Registro_Usuarios]**
- **[Testeo_DB]**

## Ejecutar pruebas

<pre>
<code>
php artisan test <font></font>
</code>
</pre>

## Pruebas de Status

En esta prueba se verifica el correcto enrutamiento de las rutas proporcionadas.

## Pruebas de Duplicacion de datos

En esta prueba se verifica que los datos proporcionados no esten duplicados desde el inicio de la ejecucion del sistema.

## Pruebas HTTP

En esta prueba tenga en cuenta que se está realizando el metodo post() desde el formulario de la vista de registro. Por lo tanto, debe pasar la contraseña por confirmación, aunque no exista en la base de datos. Y esta prueba se aplica al formulario de registro

## Prueba de base de datos

Para esta prueba vamos a usar un metodo $this->assertDatabaseHas(), que busca dentro de una base de datos y verifica si existen los datos proporcionados. 


