<?php

namespace Tests\Unit;

use App\Models\User;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_login_form()
    {
        $response = $this->get('/login');
        $response->assertStatus(200);
    }

    public function test_user_duplication()
    {
        $user1 = User::make([
                'name' => 'Raciel Hernandez',
                'email' => 'raciel@test.com'
        ]);

        $user2 = User::make([
            'name' => 'Rudy Uh',
            'email' => 'rudy@test.com'
    ]);

    $this->assertTrue($user1->name != $user2->name);
    }

    public function test_stores_new_users()
    {
        $response = $this->post('/register', [
            'name' => 'raciel',
            'email' => 'raciel@test.com',
            'Paterno' => 'Hernandez',
            'Materno' => 'Mendoza',
            'Sexo' => 'H',
            'Escolaridad' => 'Lic',
            'password' => '123456789',
            'password_confirmation' => '123456789'
        ]);

        $response->assertRedirect('/');
    }

    public function test_database()
{
    $this->assertDatabaseHas('users',[
        'name' => 'Administrador',
    ]);
}

}
